# Bronze Age: Space #

This is a total conversion mod for Bronze Age, converting a fantasy-historical city/empire builder into a space 4X. It has three main goals:

* Identify deficiencies in the Mod API
* Serve as an example for other modders.
* April fools joke

[Get Bronze Age](https://commodoreshawn.itch.io/bronze-age)

[Get the latest mod release](https://commodoreshawn.itch.io/bronze-age-space)

## Art Process ##
Most of the art is based on .model files. These are from [Asset Forge](https://assetforge.io/). The models are then exported from  Asset Forge as 2D sprites at 60% zoom for structures, and 50% zoom for units. The exported sprites are then put into the XCF files in [GIMP](https://www.gimp.org/) for positioning and tweaking, and finally exported from there as the final sprites. 

## Building ##

Bronze Age: Space can be edited and built in visual studio code, with a start task that can automatically build, and deploy the mod, and launch it in Bronze Age. You many need to adjust the paths in "package_mod.bat" and "start.bat" to suit your own environment.

The mod can also be built manually. First run tsc (typescript compile), then add all xml, js, png, and csv files under "src" to a Zip file, and add it to Bronze Age's mod folder.
