del build\BronzeAgeSpace.zip
pushd src
"C:\Program Files\7-Zip\7z.exe" a "..\build\BronzeAgeSpace.zip" "*.xml"
"C:\Program Files\7-Zip\7z.exe" a "..\build\BronzeAgeSpace.zip" "*.csv"
"C:\Program Files\7-Zip\7z.exe" a "..\build\BronzeAgeSpace.zip" "*.png"
"C:\Program Files\7-Zip\7z.exe" a "..\build\BronzeAgeSpace.zip" "*.js"
popd

butler push --userversion-file=GameVersion.txt build\BronzeAgeSpace.zip commodoreshawn/bronze-age-space:trunk