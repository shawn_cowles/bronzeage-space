/**
 * This is a local debug function, for narrowing debug messages down to just the common AI.
 * @param activeTribe The tribe currently being operated on.
 * @param debugType The type of function, see the list of debugtypes below.
 * @param msg The debug message to log.
 */
declare var ai_common_debug: (activeTribe: Tribe, debugType: string, msg: string) => void;
declare var debugtype_unit: string;
declare var debugtype_construction: string;
declare var debugtype_raid: string;
declare var debugtype_knowledge: string;
declare var unit_role_rest: number;
declare var unit_role_guard: number;
declare var unit_role_scout: number;
/**
 * Retrieve a tribe's knowledge base from storage, and make sure its format is up to date.
 * @param tribe The tribe who's knowledge base is being retrieved.
 */
declare function GetKnowledgeBase(tribe: Tribe): KnowledgeBase;
/**
 * This checks a tribe's knowledge base to make sure it's up to date. As the game updates
 * the knowlede base may change, and will have to be updated in old saves. There's no clear way
 * to do that during normal save file migration, so it's done here.
 * This is handled automatically by GetKnowledgeBase.
 * @param tribe The current tribe.
 * @param knowledgeBase The knowledge base.
 */
declare function CheckKnowledgeBaseSchema(tribe: Tribe, knowledgeBase: KnowledgeBase): void;
/**
 * Save changes to a tribe's knowledge base.
 * @param tribe The tribe who owns the knowledge base.
 * @param knowledgeBase The knowledge base to save.
 */
declare function SaveKnowledgeBase(tribe: Tribe, knowledgeBase: KnowledgeBase): void;
/**
 * Update a tribe's knowledge base. This includes what regions the tribe knows about and
 * scouting information about those regions.
 * @param tribe The tribe who owns the knowledge base.
 * @param knowledgeBase The knowledge base to update.
 */
declare function UpdateKnowledgeBase(tribe: Tribe, knowledgeBase: KnowledgeBase): void;
/**
 * Assign roles to units, and issue orders. Some portion of the tribe's units will be scouts,
 * the rest will be kept to guard their settlements.
 * @param tribe The current tribe.
 * @param knowledgeBase The tribe's knowledge base.
 * @param percScout What percent of the tribe's units shold be scouts.
 */
declare function AssignUnitRoles(tribe: Tribe, knowledgeBase: KnowledgeBase, percScout: number): void;
/**
 * Issue orders to a tribe's units. This will be called automaticalyl by AssignUnitRoles.
 * @param tribe The current tribe.
 * @param knowledgeBase The tribe's knowledge base.
 */
declare function IssueOrdersToUnits(tribe: Tribe, knowledgeBase: KnowledgeBase): void;
/**
 * Launch a raid on another tribe.
 * @param tribe The current tribe.
 * @param knowledgeBase The tribe's knowledge base.
 */
declare function LaunchRaid(tribe: Tribe, knowledgeBase: KnowledgeBase): void;
/**
 * Pick a structure from a set of structure placers, and place it in a settlement. This
 * function only places structures adjacent to two roads, resulting in rather snake-like and
 * chaotic settlement layouts.
 * @param settlement The current settlement.
 * @param constructionLocations Valid construction locations in the settlement.
 * @param instantBuild Should the structure be built instantly? This should usually be reserved for seeding settlements.
 * @param structurePlacers The structure placers for the AI.
 */
declare function PickAndPlaceStructure(settlement: Settlement, constructionLocations: Hex[], instantBuild: boolean, structurePlacers: StructurePlacer[]): Structure;
/**
 * Try to place a spectific structure in a set of potential hexes.
 * @param tribe The current tribe.
 * @param structureData The data for the structure to place.
 * @param hexes The hexes to try and place the structure in.
 * @param instantBuild Should the structure be built instantly? This should usually be reserved for seeding settlements.
 */
declare function TryToPlaceStructure(tribe: Tribe, structureData: StructureData, hexes: Hex[], instantBuild: boolean): Structure;
/**
 * The AI's remembered knowledge of the world.
 */
declare class KnowledgeBase {
    SchemaVersion: number;
    RegionKnowledge: RegionKnowledge[];
    NextRaidDate: number;
    constructor();
}
/**
 * The AI's remembered knowledge of a region.
 */
declare class RegionKnowledge {
    Id: number;
    LastScoutDate: number;
    constructor(region: Region);
}
/**
 * Structure placers define what structures the AI will build and when they will build them.
 */
interface StructurePlacer {
    /**
     * Check if this structure needs to be placed.
     * @param settlement The current settlement.
     */
    PlacementNeeded(settlement: Settlement): boolean;
    /**
     * Get the structure data associated with this placer.
     * @param settlement The current settlement.
     */
    GetStructureData(settlement: Settlement): StructureData;
}
