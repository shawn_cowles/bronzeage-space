declare class AiTag {
    static HOUSING: string;
    static ROAD: string;
    static BARRACKS: string;
    static WALL: string;
    static TOWER: string;
    static GATE: string;
    static TRADER: string;
    static SETTLER: string;
    static WARRIOR: string;
}
