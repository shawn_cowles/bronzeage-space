/*
This file contains the trigger and events for launching pirate raids.
*/
/**
 * A local debug function, for just this trigger.
 * @param msg The debug message to log.
 */
var piracy_debug = function (msg) {
    if (debugAll || true) {
        Log('piracy: ' + msg);
    }
};
var metalItem = GetItem('Metal');
var hFuelItem = GetItem('H Fuel');
var crystalsItem = GetItem('Crystals');
/**
 * This is the update function for the trigger, and is called every update.
 * @param trigger The trigger object.
 */
function t_piracy_update(trigger) {
    if (trigger.NumberProperties['raid_date'] == 0) {
        piracy_debug('setting initial raid date');
        trigger.NumberProperties['raid_date'] = determinePirateRaidDate();
    }
    else {
        if (trigger.NumberProperties['raid_date'] <= CurrentDate()) {
            trigger.NumberProperties['raid_date'] = determinePirateRaidDate();
            piracy_debug('attempting pirate raid');
            var sourceTribe = Random(GetTribes(), function (t) { return t.NumberProperties['is_pirate'] == 1 && t.Settlements.Count > 0; });
            if (sourceTribe != null) {
                var targetSettlement = determinePirateRaidTarget();
                if (targetSettlement != null) {
                    var sourceSettlement = Random(sourceTribe.Settlements.ToArray(), function (s) { return true; });
                    if (sourceSettlement != null) {
                        if (targetSettlement.Tribe == playerData.PlayerTribe) {
                            piracy_debug('  launching raid against player settlement');
                            // Create an event for talking to the player
                            var event = AddEvent('evt_piracy', 'evt_piracy_update');
                            event.NumberProperties['notification_sent'] = 0;
                            event.StringProperties['target_id'] = targetSettlement.Id;
                            event.StringProperties['source_id'] = sourceSettlement.Id;
                        }
                        else {
                            piracy_debug('  launching raid against AI settlement');
                            launchPirateRaid(sourceSettlement, targetSettlement);
                        }
                    }
                    else {
                        piracy_debug('  could not find source  settlement');
                    }
                }
                else {
                    piracy_debug('  could not find target settlement');
                }
            }
            else {
                piracy_debug('  could not find source pirate tribe');
            }
        }
    }
}
function determinePirateRaidDate() {
    // The frequency of raiding is partially random, but adjusted by world hostility
    return CurrentDate() + 16 + 16 * Math.random() * (1 - world.Hostility);
}
function determinePirateRaidTarget() {
    var potentialTargets = [];
    if (playerData.PlayerTribe != null) {
        playerData.PlayerTribe.Settlements.ToArray().forEach(function (settlement) {
            if (settlement.Structures.length > 15) {
                potentialTargets.push(settlement);
            }
        });
    }
    if (potentialTargets.length > 0) {
        return Random(potentialTargets, function (s) { return true; });
    }
    return null;
}
;
function retargetPirateRaid(event) {
    var originalTarget = GetSettlement(event.StringProperties['target_id']);
    var source = GetSettlement(event.StringProperties['source_id']);
    // If we can't find the settlement, silently end.
    if (originalTarget == null || source == null) {
        event.IsExpired = true;
        return;
    }
    piracy_debug('Pirate raid against ' + originalTarget.Tribe.Name + ' is being retargeted.');
    var potentialTargets = [];
    GetTribes().forEach(function (tribe) {
        if (tribe.NumberProperties['is_pirate'] != 1 && tribe != originalTarget.Tribe) {
            tribe.Settlements.ToArray().forEach(function (settlement) {
                if (settlement.Structures.length > 20) {
                    potentialTargets.push(settlement);
                }
            });
        }
    });
    if (potentialTargets.length > 0) {
        launchPirateRaid(source, Random(potentialTargets, function (s) { return true; }));
    }
}
function launchPirateRaid(source, target) {
    piracy_debug('Pirate raid against ' + target.Tribe.Name + ' at ' + target.Name);
    var raiderUnitTypes = source.Tribe.Race.AvailableUnits.filter(function (u) { return u.AiTags.Contains('pirate_raider'); });
    if (raiderUnitTypes.length < 1) {
        piracy_debug('  ' + source.Tribe.Name + ' doesn\'t have any raider units');
        return;
    }
    var spawnRegion = Random(target.Regions[0].Neighbors.ToArray(), function (r) { return true; });
    var spawnHex = Random(spawnRegion.Hexes, function (h) { return true; });
    var raiderCount = target.Structures.length / 10 * world.Hostility;
    piracy_debug('  structure count: ' + target.Structures.length + ' hostility: ' + world.Hostility);
    piracy_debug('  want to send ' + raiderCount + ' raiders');
    var raiders = [];
    var spawnHexes = [spawnHex];
    while (raiderCount > 0 && spawnHexes.length > 0) {
        spawnHex = spawnHexes.shift();
        raiderCount -= 1;
        raiders.push(CreateUnit(Random(raiderUnitTypes, function (ud) { return true; }), spawnHex, source.Tribe));
        spawnHex.Neighbors.ToArray().forEach(function (h) {
            if (h.Units.Count < 1) {
                spawnHexes.push(h);
            }
        });
    }
    piracy_debug('  spawned ' + raiders.length + ' raiders');
    raiders.forEach(function (unit) {
        unit.Behavior = new UnitAttackStructureBehavior(Random(target.Structures, function (s) { return true; }));
        unit.CombatStance = CombatStance.AGRESSIVE;
    });
}
;
function evt_piracy_update(event) {
    var target = GetSettlement(event.StringProperties['target_id']);
    var source = GetSettlement(event.StringProperties['source_id']);
    // If we can't find the settlement, silently end.
    if (target == null || source == null) {
        event.IsExpired = true;
        piracy_debug('target or source cannot be found, aborting event');
        return;
    }
    if (event.NumberProperties['notification_sent'] < 1) {
        piracy_debug('sending notification');
        event.NumberProperties['notification_sent'] = 1;
        var buttons = [];
        var sidebarIcons = [];
        if (metalItem != null && target.Inventory.QuantityOf(metalItem) > 100) {
            piracy_debug('  enough metal for bribe');
            buttons.push(new NotificationButton('Bribe With Metal', 'evt_piracy_bribe_metal', 'Try to bribe the pirates with 100 Metal. 50% chance'));
            sidebarIcons.push(new NotificationIconText(metalItem.IconKey, Math.floor(target.Inventory.QuantityOf(metalItem)).toString(), 'Metal supply at ' + target.Name));
        }
        if (hFuelItem != null && target.Inventory.QuantityOf(hFuelItem) > 30) {
            piracy_debug('  enough h fuel for bribe');
            buttons.push(new NotificationButton('Bribe With H Fuel', 'evt_piracy_bribe_hfuel', 'Try to bribe the pirates with 30 H Fuel. 80% chance'));
            sidebarIcons.push(new NotificationIconText(hFuelItem.IconKey, Math.floor(target.Inventory.QuantityOf(hFuelItem)).toString(), 'H Fuel supply at ' + target.Name));
        }
        if (crystalsItem != null && target.Inventory.QuantityOf(crystalsItem) > 30) {
            piracy_debug('  enough crystals for bribe');
            buttons.push(new NotificationButton('Bribe With Crystals', 'evt_piracy_bribe_crystals', 'Try to bribe the pirates with 30 Crystals. 80% chance'));
            sidebarIcons.push(new NotificationIconText(crystalsItem.IconKey, Math.floor(target.Inventory.QuantityOf(crystalsItem)).toString(), 'Crystal supply at ' + target.Name));
        }
        buttons.push(new NotificationButton('So Be It', 'evt_piracy_no_bribe', 'Let them come.'));
        AddNotification('Pirates!', 'evt_piracy', 'ui_notification_minor', [
            new NotificationBodyText('That\'s a nice colony you have there at ' + target.Name + ' it would be shame if some pirates showed up and wrecked the place.'),
            new NotificationBodyText('Wouldn\'t it?')
        ], sidebarIcons, [], buttons, 40, 'evt_piracy_timeout', true, event, target.AdminCenter.Hex.Position);
    }
}
function evt_piracy_bribe_metal(event) {
    handleBribeAttempt(event, metalItem, 100, 0.50);
}
function evt_piracy_bribe_hfuel(event) {
    handleBribeAttempt(event, hFuelItem, 30, 0.80);
}
function evt_piracy_bribe_crystals(event) {
    handleBribeAttempt(event, crystalsItem, 30, 0.80);
}
function handleBribeAttempt(event, item, qty, chance) {
    var target = GetSettlement(event.StringProperties['target_id']);
    var source = GetSettlement(event.StringProperties['source_id']);
    // If we can't find the settlement, silently end.
    if (target == null || source == null) {
        event.IsExpired = true;
        return;
    }
    if (item != null && target.Inventory.QuantityOf(item) > qty) {
        if (Math.random() < chance) {
            piracy_debug(item.Name + ' bribe succeeded');
            target.Inventory.Remove(item, qty);
            AddNotification('Bribe Accepted', 'evt_piracy', 'ui_notification_minor', [new NotificationBodyText('You make a convincing argument, we\'ll leave you alone. For now.')], [], [], [new NotificationButton('Ok', '', null)], 40, '', true, event, target.AdminCenter.Hex.Position);
            event.IsExpired = true;
            retargetPirateRaid(event);
        }
        else {
            piracy_debug(item.Name + ' bribe failed');
            AddNotification('Bribe Rejected', 'evt_piracy', 'ui_notification_minor', [new NotificationBodyText('You think us petty thieves to be bought off with such a pittance? Prepare your fleets, we are coming.')], [], [], [new NotificationButton('Ok', '', null)], 40, '', true, event, target.AdminCenter.Hex.Position);
            event.IsExpired = true;
            launchPirateRaid(source, target);
        }
    }
    else {
        piracy_debug('not enough ' + item.Name + ' for bribe');
        AddNotification('Bribe Failed', 'evt_piracy', 'ui_notification_minor', [new NotificationBodyText('Hah, you can\'t even afford to pay even such a feeble amount. Are you even worth the trouble? Let\'s find out.')], [], [], [new NotificationButton('Ok', '', null)], 40, '', true, event, target.AdminCenter.Hex.Position);
        event.IsExpired = true;
        launchPirateRaid(source, target);
    }
}
function evt_piracy_no_bribe(event) {
    var target = GetSettlement(event.StringProperties['target_id']);
    var source = GetSettlement(event.StringProperties['source_id']);
    // If we can't find the settlement, silently end.
    if (target == null || source == null) {
        event.IsExpired = true;
        return;
    }
    piracy_debug('player didn\'t try to bribe');
    event.IsExpired = true;
    launchPirateRaid(source, target);
}
function evt_piracy_timeout(event) {
    var target = GetSettlement(event.StringProperties['target_id']);
    var source = GetSettlement(event.StringProperties['source_id']);
    // If we can't find the settlement, silently end.
    if (target == null || source == null) {
        event.IsExpired = true;
        return;
    }
    AddNotification('Pirates!', 'evt_piracy', 'ui_notification_minor', [new NotificationBodyText('So arrogant that you think you can ignore us? We\'ll see about that.')], [], [], [new NotificationButton('Ok', '', null)], 40, '', true, event, target.AdminCenter.Hex.Position);
    piracy_debug('piracy event timeout');
    event.IsExpired = true;
    launchPirateRaid(source, target);
}
// Add trigger and set initial values
var t_piracy = AddTrigger('piracy', 't_piracy_update');
t_piracy.NumberProperties['raid_date'] = 0;
// Log a message to the console on load, for sanity checking
Log('evt_piracy Loaded');
