/**
 * Find an item matching a predicate. The first item in values that matches predicate will
 * be returned, or null if no item matches.
 * This is more efficient than using From.
 * @param values An array of values to choose from.
 * @param predicate A function taking in an item and returning a boolean, indicating if the item should be returned.
 */
declare function Find<T>(values: T[], predicate: (item: T) => boolean): T;
/**
 * Return a random item matching a predicate. A random item from values that matches the predicate
 * will be returned, or null if no item matches.
 * This is more efficient than using From.
 * @param values An array of values to choose from.
 * @param predicate A function taking in an item and returning a boolean, indicating if the item could be returned.
 */
declare function Random<T>(values: T[], predicate: (item: T) => boolean): T;
/**
 * Start building a complex query on a set of values using fluent notation. This can be
 * somewhat inefficient, so Find and Random should be used when possible.
 * This returns a LinqBuilder.
 * @param values An array of items to query.
 */
declare function From<T>(values: T[]): LinqBuilder<T>;
/**
 * This class allows for complex queries on a set of values using fluent notation. This can be
 * somewhat inefficient, so Find and Random should be used when possible.
 */
declare class LinqBuilder<T> {
    private sourceFunc;
    /**
     * Construct a new LinqBuilder
     * @param sourceFunc A function that returns an array of values to operate on.
     */
    constructor(sourceFunc: () => T[]);
    /**
     * Filter down the values based on a predicate.
     * @param whereClause A function that takes in an item, and returns a boolean indicating if it matched the filter.
     */
    Where(whereClause: (item: T) => boolean): LinqBuilder<T>;
    /**
     * Select new items from the values. This will return a LinqBuilder on the selected values,
     * not the originals.
     * @param selectFunc A function that takes in an item, and returns the selected value.
     */
    Select<TSelect>(selectFunc: (item: T) => TSelect): LinqBuilder<TSelect>;
    /**
     * Select new items from the values. Multiple values can be selected from each item, and
     * they will all be collapsed into a single LinqBuilder.
     * So From([0,10,20]).SelectMany(n => [n+1,n+2]).ToArray() results in [1,2,11,12,21,22]
     * @param selectFunc A function that takes in an item, and return an array of selected values.
     */
    SelectMany<TSelect>(selectFunc: (item: T) => TSelect[]): LinqBuilder<TSelect>;
    /**
     * Filter out all values that are contained in a specified array.
     * @param exceptItems An array of items to exclude.
     */
    Except(exceptItems: T[]): LinqBuilder<T>;
    /**
     * Add an array of items to the values in this LinqBuilder.
     * @param concatItems An array of items to add.
     */
    Concat(concatItems: T[]): LinqBuilder<T>;
    /**
     * Order the items.
     * @param getValFunc A function that takes an item, and returns the sort value
     */
    OrderBy(getValFunc: (item: T) => number): LinqBuilder<T>;
    Shuffle(): LinqBuilder<T>;
    /**
     * Return the first value, or null if the LinqBuilder is empty.
     */
    FirstOrNull(): T;
    /**
     * Return all items in the LinquBuilder as an array.
     */
    ToArray(): T[];
    /**
     * Return a count of the items in the LinquBuilder.
     */
    Count(): number;
    /**
     * Return a random value, or null if the LinqBuilder is empty.
     */
    RandomOrNull(): T;
    /**
     * Return the item with the lowest value, or null if the LinqBuilder is empty.
     * @param getValFunc A function taking in an item, and returning a number.
     */
    LowestOrNull(getValFunc: (item: T) => number): T;
}
declare class DeferredSettlementQueue {
    private queue;
    constructor();
    getNextSettlement(): Settlement;
    private getFromQueue();
}
