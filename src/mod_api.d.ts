﻿// CONSTANTS

declare const STRUCTURE_STATE_UNDER_CONSTRUCTION: number;
declare const STRUCTURE_STATE_CONSTRUCTED: number;
declare const STRUCTURE_STATE_UNDER_DECONSTRUCTION: number;
declare const STRUCTURE_STATE_ABANDONED: number;

// GLOBAL PROPERTIES

/**
 * Set this to true to enable script debugging
 */
declare var debugAll: boolean;

/**
 * Set this to enable debugging for only a specific Tribe's AI
 */
declare var debugTribe: Tribe;

/**
 * Set this to true to enable only a subset of debug messages
 */
declare var debugTypes: string[];

/**
 * The service holding data about the player.
 */
declare var playerData: PlayerData;

/**
 * The service holding data about the world.
 */
declare var world: WorldManager;

/**
 * This service provides UNM pattern processing for generating dialog.
 */
declare var dialogGenerator: DialogGenerator;

// GLOBAL FUNCTIONS

/**
 * Append the given {line} to the script console.
 *
 * @param obj The object to write to the script console.
 */
declare function Log(obj: any): void;

/**
 * Append the given {line} to the script console as an error.
 * @param obj The object to write to the script console.
 */
declare function LogError(obj: any): void;

/**
 * Clear the script console.
 */
declare function Clear(): void;

/**
 * Quickly reload data. This saves the game, reloads game data, then reloads the last game.
 * Script state will be cleared as part of this.
 */
declare function Reload(): void;

/**
 * Toggle the pausing of AI, event, and trigger scripts.
 */
declare function PauseScripts(): void;

/**
 * Get the current date, in seasons from from year 1.
 */
declare function CurrentDate(): number;

/**
 * Get the current year. Includes fractions of a year.
 */
declare function CurrentYear(): number;

/**
 * Add a trigger to the game, and return it.
 * @param name The name of the trigger.
 * @param updateFunc The name of the function to call for updates. Must correspond to a function in global scope.
 */
declare function AddTrigger(name: string, updateFunc: string): Trigger;

/**
 * Add an event to the game and return it.
 * @param name The name of the event.
 * @param updateFunc The name of the function to call for updates. Must correspond to a function in global scope.
 */
declare function AddEvent(name: string, updateFunc: string): Event;

/**
 * Add a function to be called late in the initialization process, after all scripts have been loaded. These functions
 * will be called ordered by the specified order.
 * @param functionName The name of the function to call.
 * @param order The order in which the function should be called.
 */
declare function AddLateInitFunction(functionName: string, order: number): void;

/**
 * Add a notification that displays a message to the player.
 * @param title The title of the notification.
 * @param body The body of the notification.
 * @param icon The icon to use for the notification.
 * @param timeout How long the notification lasts for (in realtime seconds)
 * @param closeCallback The name of the function to call when the notification closes or times out.
 * @param autoOpen Should the notification automatically open?
 * @param context The event to pass to the close callback
 * @param location The HexPosition location of the event, or null.
 */
declare function AddMessageNotification(title: string, body: string, icon: string, soundEffect: string, timeout: number, closeCallback: string, autoOpen: boolean, context: Event, location: HexPosition): void;

/**
 * Add a notification that displays asks the player a two option question.
 * @param title The title of the notification.
 * @param body The body text of the notification.
 * @param icon The icon to use for the notification.
 * @param buttonA The text for the first button of the notification.
 * @param buttonB The text for the second button of the notification.
 * @param timeout How long the notification lasts for (in realtime seconds)
 * @param callbackA The name of the function to call when the first button is clicked
 * @param callbackB The name of the function to call when the second button is clicked
 * @param timeoutCallback The name of the function to call when the notification times out
 * @param autoOpen Should the notification automatically open?
 * @param context The event to pass to the callbacks
 * @param location The HexPosition location of the event, or null.
 */
declare function AddTwoQuestionNotification(title: string, body: string, icon: string, soundEffect: string, buttonA: string, buttonB: string, timeout: number, callbackA: string, callbackB: string, timeoutCallback: string, autoOpen: boolean, context: Event, location: HexPosition): void;

/**
 * Add a notification that gives the player three options.
 * @param title The title of the notification.
 * @param body The body text of the notification.
 * @param icon The icon to use for the notification.
 * @param buttonA The text for the first button of the notification.
 * @param buttonB The text for the second button of the notification.
 * @param buttonC The text for the second button of the notification.
 * @param timeout How long the notification lasts for (in realtime seconds)
 * @param callbackA The name of the function to call when the first button is clicked
 * @param callbackB The name of the function to call when the second button is clicked
 * @param callbackC The name of the function to call when the second button is clicked
 * @param timeoutCallback The name of the function to call when the notification times out
 * @param autoOpen Should the notification automatically open?
 * @param context The event to pass to the callbacks
 * @param location The HexPosition location of the event, or null.
 */
declare function AddThreeQuestionNotification(title: string, body: string, icon: string, soundEffect: string, buttonA: string, buttonB: string, buttonC: string, timeout: number, callbackA: string, callbackB: string, callbackC: string, timeoutCallback: string, autoOpen: boolean, context: Event, location: HexPosition): void;

/**
 * Add a custom notification.
 * @param title The title of the notification.
 * @param body The body text of the notification.
 * @param icon The icon for the notification.
 * @param soundEffect The sound effect to play when the notification shows.
 * @param sideIcons An array of icon texts to display on the side of the notification.
 * @param sideButtons An array of buttons texts to display on the side of the notification.
 * @param buttons An array of buttons texts to display on the bottom of the notification.
 * @param timeout How long the notification lasts for (in realtime seconds)
 * @param timeoutCallback The name of the function to call when the notification times out
 * @param autoOpen Should the notification automatically open?
 * @param context The event to pass to the callbacks
 * @param location The HexPosition location of the event, or null.
 */
declare function AddNotification(
    title: string,
    icon: string,
    soundEffect: string,
    body: INotificationBodyItem[],
    sideIcons: NotificationIconText[],
    sideButtons: NotificationButton[],
    buttons: NotificationButton[],
    timeout: number,
    timeoutCallback: string,
    autoOpen: boolean,
    context: Event,
    location: HexPosition): void;

/**
 * Add a persistant notification that doesn't dissapear once the player has pressed a button.
 * @param title The title of the notification.
 * @param body The body text of the notification.
 * @param icon The icon for the notification.
 * @param soundEffect The sound effect to play when the notification shows.
 * @param sideIcons An array of icon texts to display on the side of the notification.
 * @param sideButtons An array of buttons texts to display on the side of the notification.
 * @param buttons An array of buttons texts to display on the bottom of the notification.
 * @param timeout How long the notification lasts for (in realtime seconds)
 * @param timeoutCallback The name of the function to call when the notification times out
 * @param autoOpen Should the notification automatically open?
 * @param context The event to pass to the callbacks
 * @param location The HexPosition location of the event, or null.
 */
declare function AddPersistantNotification(
    title: string,
    icon: string,
    soundEffect: string,
    body: INotificationBodyItem[],
    sideIcons: NotificationIconText[],
    sideButtons: NotificationButton[],
    buttons: NotificationButton[],
    timeout: number,
    timeoutCallback: string,
    autoOpen: boolean,
    context: Event,
    location: HexPosition): void;

/**
 * Add a dialog. This shows a window like a notification, but without the notification icon
 * showing in the UI.
 * @param title The title of the dialog.
 * @param body The body text of the dialog.
 * @param icon The icon for the dialog.
 * @param soundEffect The sound effect to play.
 * @param sideIcons An array of icon texts to display on the side of the dialog.
 * @param sideButtons An array of buttons texts to display on the side of the dialog.
 * @param buttons An array of buttons texts to display on the bottom of the dialog.
 */
declare function AddDialog(
    title: string,
    icon: string,
    soundEffect: string,
    body: INotificationBodyItem[],
    sideIcons: NotificationIconText[],
    sideButtons: NotificationButton[],
    buttons: NotificationButton[]): void;

/**
 * Get the region corresponding with the specified id.
 * @param regionId The Id of the region.
 */
declare function GetRegion(regionId: number): Region;

/**
 * Get the settlement corresponding with the specified id.
 * @param settlementId The Id of the settlement.
 */
declare function GetSettlement(settlementId: string): Settlement;

/**
 * Get the hexes of a region, in order of nearest to the center.
 * @param region The region.
 */
declare function HexesInOrderFromCenter(region: Region): Hex[];

/**
 * Place a structure.
 * @param hex The hex to place the structure in.
 * @param structureData The data for the structure to place.
 * @param instantBuild True to instantly finish the structure, false to have the structure build normally.
 */
declare function PlaceStructure(hex: Hex, structureData: StructureData, instantBuild: boolean): void;

/**
 * Toggle god vision for the player. God vision allows the player to see the entire world.
 */
declare function ToggleGodVision(): void;

/**
 * Start a stopwatch to log the execution time of a function, for debugging and performance monitoring.
 * Pass the performance token to StopStopwatch to log elapsed time.
 * @return A performance token.
 */
declare function StartStopwatch(): IPerformanceToken;

/**
 * Stop the stopwatch and log the elapsed time, use in conjustion with StartStopwatch.
 * @param functionName The name of the function to log.
 * @param performanceToken The performance token from StartStopwatch.
 */
declare function StopStopwatch(functionName: string, performanceToken: IPerformanceToken): void;

/**
 * Give an amount of items to the player's currently viewed settlement.
 * @param itemName The name of the item to add.
 * @param amount The amount of items to give.
 */
declare function GiveItem(itemName: string, amount: number): void;

/**
 * Print out a list of all tribes.
 */
declare function ListTribes(): void;

/**
 * Get the tribe at the specified index. Use ListTribes to get a list of tribes and indexes.
 * @param tribeIndex The index of the tribe to get.
 */
declare function GetTribe(tribeIndex: number): Tribe

/**
 * Get the tribe with a matching name. Or null if no matching tribe can be found.
 * @param tribeName The name pf the tribe to get.
 */
declare function GetTribeByName(tribeName: string): Tribe

/**
 * Get all current tribes in the world.
 */
declare function GetTribes(): Tribe[]

/**
 * Change the player's tribe.
 * @param tribe The new tribe for the player to control.
 */
declare function SetPlayerTribe(tribe: Tribe): void;

/**
 * Flood fill out to identify all walkable regions accessible from {startRegions}.
 * @param startRegions The regions to start flood filling from.
 */
declare function FloodFillWalkableRegions(startRegions: Region[]): Region[];

/**
 * Randomly pick a pathable region.
 * @param regions The regions to pick from.
 * @param unit The unit to use for checking pathability.
 */
declare function PickPathableRegion(regions: Region[], unit: Unit): Region;


/**
 * Randomly pick a pathable hex in the region.
 * @param region The region to pick from.
 * @param unit The unit to use for checking pathability.
 */
declare function PickPathableHex(region: Region, unit: Unit): Hex;

/**
 * Filter a set of hexes back to the ones that are adjacent to roads.
 * @param hexes
 * @param adjacentRoads
 */
declare function FindRoadAdjacentHexes(hexes: Hex[], adjacentRoads: number): Hex[];

/**
 * Create a new a unit in the world, and return it.
 * @param unitData The data for the new unit.
 * @param position The position for the unit.
 * @param tribe The tribe that owns the unit.
 */
declare function CreateUnit(unitData: UnitData, position: Hex, tribe: Tribe): Unit;

/**
 * Create a new tribe in the world, and return it.
 * @param race The race for the new tribe.
 */
declare function CreateTribe(race: Race): Tribe;

/**
 * Get an item by name, or null if no matching item exists.
 * @param name The name of the item.
 */
declare function GetItem(name: string): Item;

/**
 * Get an infrastructure by name, or null if no matching infrastructure exists.
 * @param name The name of the infrastructure.
 */
declare function GetInfrastructure(name: string): Infrastructure;

/**
 * Get bonus type by name, or null if no matching bonus type exists.
 * @param name The name of the bonus type.
 */
declare function GetBonusType(name: string): BonusType;

/**
 * Sort a collection of hexes by distance to a target hex.
 * @param hexes The collection to sort.
 * @param target The target to use for sorting.
 */
declare function SortByDistance(hexes: Hex[], target: Hex): Hex[];

/**
 * Get a terrain type by its ID.
 * @param terrainId The unique id of the terrain type.
 */
declare function GetTerrain(terrainId: string): Terrain;

/**
 * Add a ModButton to the UI.
 * @param button The button to add.
 */
declare function AddModButton(button: ModButton): void;

// INTERFACES

/**
 * Used by the StartStopwatch and StopStopwatch functions.
 */
declare interface IPerformanceToken
{
}

/**
 * A trigger is checked every update, intended to use for creating events.
 */
declare interface Trigger
{
    Name: string;
    UpdateFunc: string;
    StringProperties: { [key: string]: string; }
    NumberProperties: { [key: string]: number; }
}

/**
 * Events are created by triggers or other events, and are the main container of custom logic.
 */
declare interface Event
{
    Name: string;
    UpdateFunc: string;
    IsExpired: boolean;
    StringProperties: { [key: string]: string; }
    NumberProperties: { [key: string]: number; }
}

/**
 * Offers access to information about the player.
 */
declare interface PlayerData
{
    /**
     * The player's tribe.
     */
    PlayerTribe: Tribe;

    /**
     * The region of the world the player is currently looking at.
     */
    ActiveRegion: Region;

    /**
     * The settlement the player is currently looking at (or null).
     */
    ActiveSettlement: Settlement;
}

/**
 * Offers access to information about the world.
 */
declare interface WorldManager
{
    /**
     * The world generation seed.
     */
    Seed: string;

    /**
     * Get the world world hostility setting, between 0 and 1.
     */
    Hostility: number;

    /**
     * Buffs affecting the entire world.
     */
    GlobalBuffs: List<Buff>;

    /**
     * All of the tribes in the world.
     */
    Tribes: Tribe[];
}

/**
 * Provides UNM pattern processing for generating dialog.
 */
declare interface DialogGenerator
{
    GenerateDialog(pattern: string, context: string[], variables: { [name: string]: string }): string;
    GenerateName(pattern: string, context: string[]): string;
}

/**
 * Tribes are the factions of the world.
 */
declare interface Tribe
{
    /**
     * The name of the tribe.
     */
    Name: string;

    /**
     * The race of the tribe.
     */
    Race: Race;

    /**
     * All of the units the tribe currently controls.
     */
    Units: List<Unit>;

    /**
     * All of the settlements the tribe currently controls.
     */
    Settlements: List<Settlement>;

    /**
     * The name of the tribe's AI function, will be empty for the player.
     */
    AiFunction: string;

    /**
     * All of the regions the tribe currently controls (has a settlement in).
     */
    HeldRegions: Region[];

    /**
     * Is this tribe hostile to another tribe?
     */
    IsHostileTo(tribe: Tribe): boolean;

    /**
     * Buffs affecting this tribe.
     */
    Buffs: List<Buff>;

    StringProperties: { [key: string]: string; }
    NumberProperties: { [key: string]: number; }
}

/**
 * Settlements are the economic centers of the world, holding structures and producing
 * and consuming items.
 */
declare interface Settlement
{
    /**
     * The unique ID of the settlement.
     */
    Id: string;

    /**
     * The name of the settlement.
     */
    Name: string;

    /**
     * The tribe that owns this settlement.
     */
    Tribe: Tribe;

    /**
     * The Pops in this settlement
     */
    Population: List<Pop>;

    /**
     * The settlement's inventory.
     */
    Inventory: ItemInventory;

    /**
     * The trade routes originating from this settlement.
     */
    TradeRoutes: List<TradeRoute>;

    /**
     * How much housing is available in the settlement.
     */
    AvailableHousing: number;

    /**
     * All of the structures in the settlement.
     */
    Structures: Structure[];

    /**
     * The admin center of the settlement.
     */
    AdminCenter: Structure;

    /**
     * All of the current construction locations in the settlement.
     */
    ConstructionLocations: Hex[];

    /**
     * All of the regions this settlement controls.
     */
    Regions: Region[];

    /**
     * The number of free population in the settlement.
     */
    FreePopulation: number;

    /**
     * Get the net infrastructure supply in the settlement.
     */
    GetInfrastructure(infrastructure: Infrastructure): number;

    /**
     * Get the current item production rate in the settlement.
     */
    GetItemRate(item: Item): number;

    /**
     * The list of stat displays for the settlement.
     */
    StatDisplays: List<StatDisplay>;

    StringProperties: { [key: string]: string; }
    NumberProperties: { [key: string]: number; }
}

/**
 * An ItemInventory stores items.
 */
declare interface ItemInventory
{
    /**
     * Check for the quantity of an item in the inventory.
     */
    QuantityOf(item: Item): number;

    /**
     * Add items to the inventory.
     */
    Add(item: Item, qty: number): void;

    /**
     * Remove items from the inventory.
     */
    Remove(item: Item, qty: number): void;
}

/**
 * A type of item, like wheat or mudbrick.
 */
declare interface Item
{
    /**
     * The name of the item.
     */
    Name: string;

    /**
     * The name of the icon image for the item.
     */
    IconKey: string;

    /**
     * The description of the item.
     */
    Description: string;
}

/**
 * A type of bonus that can affect structures.
 */
declare interface BonusType
{
    /**
     * The name of the bonus type.
     */
    Name: string;

    /**
     * The name of the icon image for the bonus type.
     */
    IconKey: string;
}

/**
 * Regions are chunks of the world, grouping a set of Hexes that share the same biome.
 */
declare interface Region
{
    /**
     * The unique Id of the region.
     */
    Id: number;

    /**
     * The name of the region.
     */
    Name: string;

    /**
     * The center hex position of the region.
     */
    Center: HexPosition;

    /**
     * All of the hexes in the region.
     */
    Hexes: Hex[];

    /**
     * The neighboring regions of this region.
     */
    Neighbors: List<Region>;

    /**
     * The settlement in this region (or null)
     */
    Settlement: Settlement;

    /**
     * Is any part of the region walkable?
     */
    IsWalkable: boolean;

    /**
     * Is any part of the region swimmable?
     */
    IsSwimmable: boolean;

    /**
     * Could a unit potentially be able to cross this region?
     */
    IsPotentiallyPathable(unit: Unit): boolean;

    /**
     * Buffs affecting this region.
     */
    Buffs: List<Buff>;
}

/**
 * Hexes are the basic unit of space of the world.
 */
declare interface Hex
{
    /**
     * The position of the hex.
     */
    Position: HexPosition;

    /**
     * The region the hex belongs to.
     */
    Region: Region;

    /**
     * The structure on the hex (or null)
     */
    Structure: Structure;

    /**
     * The neighbors of the hex, some will be null on the edges of the world.
     */
    Neighbors: List<Hex>;

    /**
     * The units present in the hex.
     */
    Units: List<Unit>;

    /**
     * The terrain in the hex.
     */
    Terrain: Terrain;

    /**
     * Is the hex pathable by a unit?
     */
    IsPathable(unit: Unit, pathThroughHostileStructures: boolean): boolean;

    /**
     * Is the hex pathable by a type of unit? This doesn't consider structures allowing allies through.
     */
    IsPathable(unitData: UnitData): boolean;
}

/**
 * A position of a hex in the world.
 */
declare interface HexPosition
{
    /**
     * Distance to another hex, in hex widths
     */
    DistanceTo(destination: HexPosition): number;
}

/**
 * An individual structure in a settlement.
 */
declare interface Structure
{
    /**
     * The backing data of the structure type.
     */
    Data: StructureData;

    /**
     * The hex containing this structure.
     */
    Hex: Hex;

    /**
     * The construction timer of the structure.
     */
    ConstructionProgress: number;

    /**
     * How much damage the structure has recieved.
     */
    Damage: number;

    /**
     * The health percent of the structure.
     */
    HealthPercent: number;

    /**
     * The effective health the structure, taking into account construction progress.
     */
    EffectiveHealth: number;

    /**
     * The construction percent of the structure.
     */
    ConstructionPercent: number;

    /**
     * Is the structure currently operational?
     */
    IsOperational: boolean;

    /**
     * The settlement this structure belongs to (or null).
     */
    Settlement: Settlement;

    /**
     * The unit currently inside the structure (or null)
     */
    UnitInside: Unit;

    /**
     * The current state of the structure (see the STRUCTURE_STATE constants)
     */
    State: number;

    UpgradingTo: StructureData;

    UpgradeProgress: number;
}

/**
 * A unit, such as a warband or settlers.
 */
declare interface Unit
{
    /**
     * The backing data of the unit type.
     */
    Data: UnitData;

    /**
     * The hex the unit is occupying (or null).
     */
    Hex: Hex;

    /**
     * The unit's current behavior.
     */
    Behavior: IUnitBehavior;

    /**
     * The tribe that owns the unit.
     */
    Tribe: Tribe;

    /**
     * The unit's inventory.
     */
    Inventory: ItemInventory;

    /**
     * The damage the unit has suffered.
     */
    Damage: number;

    /**
     * The unit's current strength percent.
     */
    StrengthPercent: number;

    /**
     * Should the unit be deleted and removed?
     */
    FlaggedForRemoval: boolean;

    /**
     * The unit's current combat stance.
     */
    CombatStance: CombatStance;

    /**
     * The structure currently holding the unit (or null)
     */
    ContainingStructure: Structure;

    /**
     * The structure that supports the unit (or null)
     */
    SupportingStructure: Structure;

    /**
     * The hexes this unit can see.
     */
    VisibleHexes: Hex[];

    StringProperties: { [key: string]: string; }
    NumberProperties: { [key: string]: number; }
}

/**
 * A race in the world, like the humans or masklings.
 */
declare interface Race
{
    /**
     * The unique ID of the race.
     */
    Id: string;

    /**
     * The name of the race.
     */
    Name: string;

    /**
     * The base growth time (in seasons).
     */
    SeasonsToReproduce: number;

    /**
     * What type of infrastructure the race uses for housing.
     */
    HousingType: Infrastructure;

    /**
     * All of the types of units the race has access to.
     */
    AvailableUnits: UnitData[];

    /**
     * All of the types of structures the race has access to build (doesn't include upgrades).
     */
    AvailableStructures: StructureData[];

    IdlePopKey: string;

    WorkingPopKey: string;

    /**
     * The list of AI tags defined in the data file.
     */
    AiTags: List<string>;
}

/**
 * Type information for a structure.
 */
declare interface StructureData
{
    /**
     * The name of the structure type.
     */
    TypeName: string;

    /**
     * Is this structure type an admin center?
     */
    IsAdminCenter: boolean;

    /**
     * Is this structure type walkable?
     */
    IsWalkable: boolean;

    /**
     * Is this structure type swimmable?
     */
    IsSwimmable: boolean;

    /**
     * The type of unit this structure supports (or null)
     */
    SupportedUnit: UnitData;

    /**
     * The structure this structure can upgrade to (or null).
     */
    UpgradesTo: StructureData;

    /**
     * Determine if this type of structure can be placed in a hex.
     * @param hex The hex to test.
     */
    CanBePlaced(hex: Hex): boolean;

    /**
     * Get structure's supply of the specified infrastructure.
     */
    GetRawInfrastructureSupply(name: string): number;

    /**
     * Get structure's supply of the specified item.
     */
    GetRawItemSupply(name: string): number;

    /**
     * Get structure's consumption of the specified item.
     */
    GetRawItemConsumption(name: string): number;

    /**
     * Get structure's supply of the specified infrastructure.
     */
    GetInfrastructureSupply(infrastructure: Infrastructure): number;

    /**
     * The list of AI tags defined in the data file.
     */
    AiTags: List<string>;
}

/**
 * Type information for a unit.
 */
declare interface UnitData
{
    /**
     * The name of the unit type.
     */
    TypeName: string;

    /**
     * Can the unit walk?
     */
    CanWalk: boolean;

    /**
     * Can the unit swim?
     */
    CanSwim: boolean;

    /**
     * Can the unit found or join settlements?
     */
    CanSettle: boolean;

    /**
     * Can the unit trade?
     */
    CanTrade: boolean;

    /**
     * Can the unit capture other settlements?
     */
    CanCapture: boolean;


    //Combat Stats
    Strength: number;
    Defense: number;
    Vision: number;
    HasMeleeAttack: boolean;
    MeleeAttack: number;
    HasRangedAttack: boolean;
    RangedAttack: number;
    RangedAttackRange: number;

    /**
     * The list of AI tags defined in the data file.
     */
    AiTags: List<string>;
}

/**
 * A combat stance for a unit.
 */
declare class CombatStance
{
    static AGRESSIVE: CombatStance;
    static DEFENSIVE: CombatStance;
    static SKIRMISH: CombatStance;
    static PASSIVE: CombatStance;
    static DEFAULT: CombatStance;
}

/**
 * Behavior controlling how the unit behaves.
 */
declare interface IUnitBehavior
{
    /**
     * The name of the behavior.
     */
    Name: string;

    /**
     * Is the behavior finished?
     */
    IsFinished: boolean;

    /**
     * Is the behavior a combat behavior (attacking a unit or structure)
     */
    IsCombatBehavior: boolean;
}

/**
 * A generic C# list, of type T.
 */
interface List<T>
{
    ToArray(): T[];
    Count: number;
    Add(newItem: T): void;
    RemoveAt(index: number);
    Contains(item: T): boolean;
    Remove(item: T): void;
}

/**
 * Infrastructure is an abstract representation of the support of the settlement.
 */
interface Infrastructure
{
    /**
     * The name of the infrastructure.
     */
    Name: string;
}

interface Terrain
{
    Id: string;
    IsWalkable: boolean;
    IsSwimmable: boolean;
    MoveCost: number;
    Resource: Resource;
}

interface Resource
{
    Name: string;
}

// CLASSES

/**
 * Pops are the abstract representation of a settlement population, they are needed to
 * operate structures, and train units.
 */
declare class Pop
{
    /**
     * Is the pop currently operating a building.
     */
    IsWorking: boolean;

    constructor(tribe: Tribe);
}

/**
 * Behavior for a unit that's waiting for other orders.
 */
declare class UnitIdleBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitIdleBehavior
     */
    constructor();
}

/**
 * Behavior for a unit that's moving to a location.
 */
declare class UnitMoveBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitMoveBehavior
     * @param destination The destination to move to.
     */
    constructor(destination: Hex);
}

/**
 * Behavior for a unit that's either joining a settlement, or creating a new one.
 */
declare class UnitSettleBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitSettleBehavior
     * @param destination The destination to found the new settlement at, or to join the exising settlement at.
     */
    constructor(destination: Hex);
}

/**
 * Behavior for a unit that's doing trade.
 */
declare class UnitTradeBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitTradeBehavior
     * @param targetSettlement The settlement to trade for.
     */
    constructor(targetSettlement: Settlement);
}

/**
 * Behavior for a unit that's trying to steal items from a settlement.
 */
declare class UnitRaidBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitRaidBehavior
     * @param region The target region, the unit will try to steal items from a hostile settlement in this region.
     */
    constructor(region: Region);
}

/**
 * Behavior for a unit that's resting at its supporting structure.
 */
declare class UnitRestBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitRestBehavior
     */
    constructor();
}

/**
 * Behavior for a unit that's trying to capture a settlement.
 */
declare class UnitCaptureBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitCaptureBehavior
     * @param settlement The settlement to capture.
     */
    constructor(settlement: Settlement);
}

/**
 * Behavior for a unit that's attacking a structure.
 */
declare class UnitAttackStructureBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitAttackStructureBehavior
     * @param target The structure to attack.
     */
    constructor(target: Structure);
}

/**
 * Behavior for a unit that's rampaging around, destroying everything it sees.
 */
declare class UnitRampageBehavior implements IUnitBehavior
{
    Name: string;
    IsFinished: boolean;
    IsCombatBehavior: boolean;

    /**
     * Construct a new UnitRampageBehavior.
     */
    constructor();
}

/**
 * A base type for any notification body item. Only the NotificationBodyText or
 * NotificationBodyStatBlock implementations are supported.
 */
declare interface INotificationBodyItem
{
}

/**
 * A basic block of text for the notification.
 */
declare class NotificationBodyText implements INotificationBodyItem 
{
    public Text: string;

    constructor(text: string);
}

/**
 * A set of icons and text for the notification, they will be displayed in a grid, 3 rows wide.
 */
declare class NotificationBodyStatBlock implements INotificationBodyItem 
{
    public Contents: NotificationIconText[];

    constructor(contents: NotificationIconText[]);
}

/**
 * A small icon paired with text, and an optional tooltip. Can be displayed in
 * NotificationBodyStatBlock or the side of a notification.
 */
declare class NotificationIconText 
{
    Icon: string;
    Text: string;
    Tooltip: string;

    constructor(icon: string, text: string, tooltip: string);
}

/**
 * A button for a notification. It can have an optional tooltip. Can be displayed in the side
 * of a notification, or along the bottom.
 */
declare class NotificationButton
{
    Text: string;
    Callback: string;
    Tooltip: string;

    constructor(text: string, callback: string, tooltip: string);
}

/**
 * Defines a trade route taking items from one settlement to another.
 */
declare class TradeRoute
{
    /**
     * The destination settlement for the trade route.
     */
    Destination: Settlement;

    /**
     * Is the trade route swimmable.
     */
    SwimmablePath: boolean;

    /**
     * Is the trade route walkable.
     */
    WalkablePath: boolean;

    /**
     * The list of items exported along the trade route.
     */
    ExportedItems: Item[];

    /**
     * The backlog of items waiting to be carried along the trade route.
     */
    TotalBacklog: number;

    constructor(destination: Settlement);

    /**
     * Get the desired exports per season along the trade route for a specified item.
     */
    GetExportsPerSeasonFor(item: Item): number;

    /**
     * Set the desired exports per season along the trade route for a specified item.
     */
    SetExportsPerSeasonFor(item: Item, qty: number): void;
}

/**
 * Defines a buff that can affect item production, infrastructure, and buffs. Can be applied
 * to a region, a tribe, or the whole world.
 */
declare class Buff
{
    /**
     * The image key for the icon to display in the UI for this buff.
     */
    IconKey: string;

    /**
     * The name of this buff.
     */
    Name: string;

    /**
     * Text to show in a tooltip for this buff.
     */
    Tooltip: string;

    StringProperties: { [key: string]: string; }
    NumberProperties: { [key: string]: number; }

    /**
     * Construct a new buff.
     * @param iconKey The image key for the icon to display in the UI for this buff
     * @param name The name of this buff
     * @param tooltip Text to show in a tooltip for this buff
     */
    constructor(iconKey: string, name: string, tooltip: string);

    /**
     * Get the per-season item production this buff provides of the specified item
     * @param item The item to get the production rate for
     */
    GetItemProduction(item: Item): number;

    /**
     * Set the per-season item production this buff provides. All settlements affected by this buff will get this production. This can be negative.
     * @param item The item to produce
     * @param perSeason The per season produciton rate
     */
    SetItemProduction(item: Item, perSeason: number): void;

    /**
     * Get the bonus amount of infrastructure this buff provides, for the specified infrastructure
     * @param infrastructure The infrastructure to get the bonus amount of
     */
    GetInfrastructure(infrastructure: Infrastructure): number;

    /**
     * Set the bonus amount of infrastructure this buff provides. This can be negative.
     * @param infrastructure The infrastructure to set the rate for.
     * @param quantity The bonus infrastructure. Should be an integer.
     */
    SetInfrastructure(infrastructure: Infrastructure, quantity: number): void;

    /**
     * Get the amount of bonus this buff provides, for a specified bonus type.
     * @param bonusType The bonus type to check.
     */
    GetBonus(bonusType: BonusType): number;

    /**
     * Set the amount of bonus this buff provides. This can be negative.
     * @param bonusType The bonus type.
     * @param quantity The amount provided by the buff. Should be an integer.
     */
    SetBonus(bonusType: BonusType, quantity: number): void;
}

/**
 * An arbitrary stat display that can be shown for settlements or units.
 */
declare class StatDisplay
{
    /**
     * The id of the stat display, for identification in scripts, readonly.
     */
    Id: string;

    /**
     * The filename of the icon to display, readonly.
     */
    IconKey: string;

    /**
     * The text to display, can be changed.
     */
    Text: string;

    /**
     * The tooltip to show on hover, readonly.
     */
    Tooltip: string;

    /**
     * Create a new StatDisplay.
     * @param id The id of the stat display, for identification in scripts.
     * @param iconKey The filename of the icon to display.
     * @param text The text to display.
     * @param tooltip The tooltip to show on hover.
     */
    constructor(id: string, iconKey: string, text: string, tooltip: string);
}

/**
 * A button that can be shown in the UI and will call a script function when clicked.
 */
declare class ModButton
{
    /**
     * The key of the icon to display.
     */
    IconKey: string;

    /**
     * The tooltip to show when hovered.
     */
    Tooltip: string;

    /**
     * The function to call when the button is clicked.
     */
    ClickFunction: string;

    /**
     * The function to call to check if the button should be visible.
     */
    IsVisibleFunction: string;

    /**
     * The function to call to check if the button should be disabled.
     */
    IsDisabledFunction: string;

    /**
     * Create a new ModButton
     * @param iconKey The key of the icon to display.
     * @param tooltip The tooltip to show when hovered.
     * @param clickFunction The function to call when the button is clicked.
     * @param isVisibleFunction The function to call to check if the button should be visible.
     * @param isDisabledFunction The function to call to check if the button should be disabled.
     */
    constructor(
        iconKey: string,
        tooltip: string,
        clickFunction: string,
        isVisibleFunction: string,
        isDisabledFunction: string);
}