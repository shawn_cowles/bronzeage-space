/*
This file contains the AI for the AI pirates.
*/

/**
 * This is a local debug function, for narrowing debug messages down to just the pirate AI.
 * @param activeTribe The tribe currently being operated on.
 * @param msg The debug message to log.
 */
var pirate_ai_debug = function (activeTribe: Tribe, msg: string) {
    if (debugAll || debugTribe == activeTribe) {
        Log('pirate_ai: ' + msg);
    }
}

/**
 * This function is called during world generation to place the structures for the pirate hideout.
 * @param tribe The tribe being seeded.
 */
function pirate_seed(tribe: Tribe) {

    tribe.NumberProperties['is_pirate'] = 1;

    pirate_ai_debug(tribe, 'Determining admin center data.');
    // Find structure data for an admin center.
    var adminCenterData = Find(
        tribe.Race.AvailableStructures,
        sd => sd.IsAdminCenter);

    var supportingStructures = tribe.Race.AvailableStructures
        .filter(sd => !sd.IsAdminCenter);

    if (adminCenterData == null) {
        pirate_ai_debug(tribe, 'No admin center data found, aborting seed.');
        return;
    }

    // For each settlement...
    var settlements = tribe.Settlements.ToArray();
    for (var i = 0; i < settlements.length; i++) {
        var settlement = settlements[i];
        pirate_ai_debug(tribe, 'Seeding settlement ' + i + ': ' + settlement.Name);

        var hexesInOrder = HexesInOrderFromCenter(settlement.Regions[0]);

        // ...place an admin center...
        pirate_ai_debug(tribe, '  placing admin center')
        var adminCenter = TryToPlaceStructure(tribe, adminCenterData, hexesInOrder, true);

        if (adminCenter != null) {
            // ...then place a collection of satellite structures
            var structureCount = Math.random() * 3 + 2;

            for (var i = 0; i < structureCount && supportingStructures.length > 0; i++) {

                TryToPlaceStructure(
                    tribe,
                    Random(supportingStructures, sd => true),
                    hexesInOrder,
                    true);
            }
        } else {
            pirate_ai_debug(tribe, '  could not place admin center.');
        }
    }
}

/**
 * This function is called every 20 seconds or so. It's responsible for enemy tribe AI.
 * @param tribe The tribe for the AI to control.
 */
function pirate_ai(tribe: Tribe): void {
    var performanceToken = StartStopwatch();
    try {

        tribe.Units.ToArray().forEach(unit => {
            // Reassign idle units 
            if (unit.Behavior.IsFinished) {

                // if the unit has a supporting structure, patrol
                if (unit.SupportingStructure != null && unit.SupportingStructure.Settlement != null) {
                    var destination = Random(unit.SupportingStructure.Settlement.Structures, s => true).Hex;
                    destination = Random(destination.Neighbors.ToArray(), h=> h.IsPathable(unit, false));
                    unit.Behavior = new UnitMoveBehavior(destination);
                    unit.CombatStance = CombatStance.AGRESSIVE;
                } else {
                    // if units are in a friendly region ...
                    if (unit.Hex.Region.Settlement != null
                        && unit.Hex.Region.Settlement.Tribe == unit.Tribe) {

                        var localSettlement = unit.Hex.Region.Settlement;

                        var hexWithHostile = Find(unit.Hex.Region.Hexes,
                            h => h.Units.ToArray()
                                .filter(u => u.Tribe != unit.Tribe).length > 0);
                        // ... order them to attack hostiles ...
                        if (hexWithHostile != null) {
                            unit.Behavior = new UnitMoveBehavior(hexWithHostile);
                            unit.CombatStance = CombatStance.AGRESSIVE;
                        } else {
                            // ... otherwise "remove" the unit by killing it, a little odd but it works
                            unit.Damage = 100000;
                        }
                    } else {

                        // If the unit's tribe has settlements fly home
                        if (unit.Tribe.Settlements.Count > 0) {
                            unit.Behavior = new UnitMoveBehavior(unit.Tribe.Settlements.ToArray()[0].AdminCenter.Hex);
                            unit.CombatStance = CombatStance.AGRESSIVE;
                        } else {
                            // Otherwise just hang out and be angry
                            unit.Behavior = new UnitRampageBehavior();
                            unit.CombatStance = CombatStance.AGRESSIVE;
                        }
                    }
                }
            }
        });
    }
    catch (e) {
        LogError('Exception in pirate_ai: ' + e.message);
    }

    StopStopwatch('pirate_ai', performanceToken);
};

// Log a message to the console on load, for sanity checking
Log('sa_pirate_ai Loaded');