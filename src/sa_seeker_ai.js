/*
This file contains the AI for the AI Seekers.
*/
/**
 * This is a local debug function, for narrowing debug messages down to just the seeker AI.
 * @param activeTribe The tribe currently being operated on.
 * @param msg The debug message to log.
 */
var seeker_ai_debug = function (activeTribe, msg) {
    if (debugAll || debugTribe == activeTribe) {
        Log('seeker_ai: ' + msg);
    }
};
var seekerTagShipyard = 'Shipyard';
var seekerTagCrystalizer = 'Crystalizer';
/**
 * This function is called during world generation to place the initial structures in a tribe's settlements.
 * @param tribe The tribe being seeded.
 */
function seeker_seed(tribe) {
    seeker_ai_debug(tribe, 'Determining admin center data.');
    // Find structure data for an admin center.
    var adminCenterData = Find(tribe.Race.AvailableStructures, function (sd) { return sd.IsAdminCenter; });
    if (adminCenterData == null) {
        seeker_ai_debug(tribe, 'No admin center data found, aborting seed.');
        return;
    }
    // For each settlement, place an admin center. 
    // The AI will then flesh out the settlements during game play similar to the player.
    var settlements = tribe.Settlements.ToArray();
    for (var i = 0; i < settlements.length; i++) {
        var settlement = settlements[i];
        seeker_ai_debug(tribe, 'Seeding settlement ' + i + ': ' + settlement.Name);
        seeker_ai_debug(tribe, ' population: ' + settlement.Population.Count);
        var hexesInOrder = HexesInOrderFromCenter(settlement.Regions[0]);
        seeker_ai_debug(tribe, '  placing admin center');
        var adminCenter = TryToPlaceStructure(tribe, adminCenterData, hexesInOrder, true);
        if (adminCenter == null) {
            seeker_ai_debug(tribe, '  could not place admin center.');
        }
    }
}
var powerInf = GetInfrastructure('Power');
var happinessInf = GetInfrastructure('Happiness');
var noblesInf = GetInfrastructure('Nobles');
var seeker_structure_placers = [
    // Build solar collectors if power is low
    {
        PlacementNeeded: function (settlement, plan) {
            return settlement.GetInfrastructure(powerInf) < 4;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return !sd.IsAdminCenter && sd.GetInfrastructureSupply(powerInf) > 0; });
        }
    },
    // Build houses if there isn't spare housing
    {
        PlacementNeeded: function (settlement, plan) {
            return settlement.AvailableHousing < 1;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return sd.AiTags.Contains(AiTag.HOUSING); });
        }
    },
    // Build temples if morale is low
    {
        PlacementNeeded: function (settlement, plan) {
            return settlement.GetInfrastructure(happinessInf) < settlement.Population.Count;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return !sd.IsAdminCenter && sd.GetInfrastructureSupply(happinessInf) > 0; });
        }
    },
    // Build turrets if the turret to structure ratio falls below 10%
    {
        PlacementNeeded: function (settlement, plan) {
            return plan.turretCount / settlement.Structures.length < 0.1;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return sd.AiTags.Contains(AiTag.TOWER); });
        }
    },
    // Build a shipyard if the shipyard to swarmyard ratio falls below 25%
    {
        PlacementNeeded: function (settlement, plan) {
            return plan.swarmyardCount > 0 && plan.shipyardCount / plan.swarmyardCount < 0.25 && settlement.GetInfrastructure(noblesInf) > 0;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return sd.TypeName == 'Shipyard'; });
        }
    },
    // Build a swarmyard if the swarmyard to structure ratio falls below 5%, and there are mines
    {
        PlacementNeeded: function (settlement, plan) {
            return plan.swarmyardCount / settlement.Structures.length < 0.05 && plan.swarmyardCount < plan.mineCount;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return sd.TypeName == 'Swarmyard'; });
        }
    },
    // Build mines if there is an available ore patch
    {
        PlacementNeeded: function (settlement, plan) {
            return Find(plan.oreHexes, function (h) { return h.Structure == null; }) != null;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return sd.TypeName == 'Mine'; });
        }
    },
    // Build crystalizers if there is an available tritium patch
    {
        PlacementNeeded: function (settlement, plan) {
            return Find(plan.tritiumHexes, function (h) { return h.Structure == null; }) != null;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return sd.TypeName == 'Crystalizer'; });
        }
    },
    // TODO build noble estates
    // Build a hyperalloy foundry if hyperalloy foundry to crystalizer ratio falls below 30%
    {
        PlacementNeeded: function (settlement, plan) {
            return plan.crystalizerCount > 0 && plan.hyperalloyCount / plan.crystalizerCount < 0.3;
        },
        GetStructureData: function (settlement) {
            return Find(settlement.Tribe.Race.AvailableStructures, function (sd) { return sd.TypeName == 'Hyperalloy Foundry'; });
        }
    },
];
var seekerPlans = {};
/**
 * This function is called every 20 seconds or so. It's responsible for enemy tribe AI.
 * @param tribe The tribe for the AI to control.
 */
function seeker_ai(tribe) {
    var performanceToken = StartStopwatch();
    try {
        // Settlement Management
        var settlements = tribe.Settlements.ToArray();
        for (var i = 0; i < settlements.length; i++) {
            var settlement = settlements[i];
            // Get or build the construction plan
            if (!seekerPlans[settlement.Id]) {
                seeker_ai_debug(tribe, 'Generating plan for: ' + settlement.Name);
                seekerPlans[settlement.Id] = new SeekerConstructionPlan(settlement);
            }
            var plan = seekerPlans[settlement.Id];
            plan.updateCounts();
            // For each settlement, if there is free population and room, try to place a structure.
            if (settlement.FreePopulation > 0
                && plan.walkableHexCount > 0
                && settlement.Structures.length < 150) {
                // TODO find structures to upgrade, once that is exposed in the API
                var adminCenter = settlement.AdminCenter;
                var constructionLocations = plan.getConstructionLocations();
                // Use the structure placers defined above to figure out what structure is needed, and place one.
                var placedStructure = SeekerPlaceStructure(settlement, constructionLocations, false, seeker_structure_placers, plan);
                if (placedStructure != null) {
                    seeker_ai_debug(tribe, 'placed ' + placedStructure.Data.TypeName + ' in ' + settlement.Name);
                }
            }
            tribe.Units.ToArray().forEach(function (unit) {
                if (unit.Behavior == null || unit.Behavior.IsFinished) {
                    if (unit.SupportingStructure != null && unit.SupportingStructure.Settlement != null) {
                        var destination = Random(unit.SupportingStructure.Settlement.Structures, function (s) { return true; }).Hex;
                        destination = Random(destination.Neighbors.ToArray(), function (h) { return h.IsPathable(unit, false); });
                        unit.Behavior = new UnitMoveBehavior(destination);
                        unit.CombatStance = CombatStance.AGRESSIVE;
                    }
                }
            });
        }
    }
    catch (e) {
        LogError('Exception in seeker_ai: ' + e.message);
    }
    StopStopwatch('seeker_ai', performanceToken);
}
;
function SeekerPlaceStructure(settlement, constructionLocations, instantBuild, structurePlacers, plan) {
    var performanceToken = StartStopwatch();
    var placedStructure = null;
    try {
        var performanceToken2 = StartStopwatch();
        // Determine what structures are needed.
        var neededStructures = From(structurePlacers)
            .Where(function (sp) { return sp.PlacementNeeded(settlement, plan); })
            .Select(function (sp) { return sp.GetStructureData(settlement); })
            .Where(function (sd) { return sd != null; })
            .ToArray();
        StopStopwatch('ss determine count', performanceToken2);
        seeker_ai_debug(settlement.Tribe, "seeker place structures");
        if (neededStructures.length > 0) {
            seeker_ai_debug(settlement.Tribe, "  needed structures: " + neededStructures.length);
            seeker_ai_debug(settlement.Tribe, "  construction locations: " + constructionLocations.length);
            // Try to place a structure in the settlement.
            for (var i = 0; i < neededStructures.length && placedStructure == null; i++) {
                performanceToken2 = StartStopwatch();
                // Try to not place on a resource, if possible
                var nonResourcePositions = constructionLocations.filter(function (h) { return h.Terrain.Resource == null && neededStructures[i].CanBePlaced(h); });
                seeker_ai_debug(settlement.Tribe, "  nonResourcePositions: " + nonResourcePositions.length);
                seeker_ai_debug(settlement.Tribe, "  trying to place: " + neededStructures[i].TypeName);
                if (nonResourcePositions.length > 0) {
                    placedStructure = TryToPlaceStructure(settlement.Tribe, neededStructures[i], nonResourcePositions, instantBuild);
                }
                else {
                    placedStructure = TryToPlaceStructure(settlement.Tribe, neededStructures[i], constructionLocations, instantBuild);
                }
                if (placedStructure != null) {
                    plan.addStructureToCounts(placedStructure);
                    seeker_ai_debug(settlement.Tribe, "  placed: " + neededStructures[i].TypeName);
                }
                StopStopwatch('ss trying to place', performanceToken2);
            }
        }
    }
    catch (e) {
        LogError('Exception in SeekerPlaceStructure: ' + e.message);
    }
    StopStopwatch('SeekerPlaceStructure', performanceToken);
    return placedStructure;
}
var SeekerConstructionPlan = /** @class */ (function () {
    function SeekerConstructionPlan(settlement) {
        this.settlement = settlement;
        this.oreHexes = settlement.ConstructionLocations
            .filter(function (h) { return h.Terrain.Resource != null && h.Terrain.Resource.Name == 'Ore'; });
        this.tritiumHexes = settlement.ConstructionLocations
            .filter(function (h) { return h.Terrain.Resource != null && h.Terrain.Resource.Name == 'Tritium'; });
        this.orderedHexesInRegion = SortByDistance(settlement.Regions[0].Hexes.filter(function (h) { return h.Terrain.IsWalkable; }), settlement.AdminCenter.Hex);
    }
    SeekerConstructionPlan.prototype.updateCounts = function () {
        // Safe assumption that nothing's changed if the count hasn't changed. Unlikely for construction 
        // and destruction to be simultaneous, and this will save a few ms each update.
        if (this.lastStructureCount == this.settlement.Structures.length) {
            return;
        }
        this.walkableHexCount = this.settlement.ConstructionLocations.filter(function (h) { return h.Terrain.IsWalkable; }).length;
        var performanceToken = StartStopwatch();
        this.turretCount = 0;
        this.swarmyardCount = 0;
        this.shipyardCount = 0;
        this.mineCount = 0;
        this.crystalizerCount = 0;
        this.hyperalloyCount = 0;
        this.lastStructureCount = 0;
        for (var i = 0; i < this.settlement.Structures.length; i++) {
            var structure = this.settlement.Structures[i];
            this.addStructureToCounts(structure);
        }
        StopStopwatch('SeekerConstructionPlan.updateCounts', performanceToken);
    };
    SeekerConstructionPlan.prototype.addStructureToCounts = function (structure) {
        this.lastStructureCount += 1;
        this.walkableHexCount -= 1;
        if (structure.Data.AiTags.Contains(AiTag.TOWER)) {
            this.turretCount += 1;
        }
        if (structure.Data.TypeName == 'Swarmyard') {
            this.swarmyardCount += 1;
        }
        if (structure.Data.AiTags.Contains(seekerTagShipyard)) {
            this.shipyardCount += 1;
        }
        if (structure.Data.TypeName == 'Mine') {
            this.mineCount += 1;
        }
        if (structure.Data.AiTags.Contains(seekerTagCrystalizer)) {
            this.crystalizerCount += 1;
        }
        if (structure.Data.TypeName == 'Hyperalloy Foundry') {
            this.hyperalloyCount += 1;
        }
    };
    SeekerConstructionPlan.prototype.getConstructionLocations = function () {
        return this.orderedHexesInRegion.filter(function (h) { return h.Structure == null; });
    };
    return SeekerConstructionPlan;
}());
// Log a message to the console on load, for sanity checking
Log('sa_seeker_ai Loaded');
